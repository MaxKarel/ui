import pandas as pd
import numpy as np
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
from sklearn.externals import joblib
from sklearn.metrics import accuracy_score
import sys, getopt


def print_help():
    print("THis is help")


def print_statistics(y_true, y_pred, title):
    from sklearn.metrics import confusion_matrix
    import seaborn as sn
    cm = confusion_matrix(y_true, y_pred)
    print(cm)
    plt.figure(figsize = (10,7))
    sn.set(font_scale=1.4)#for label size
    sn.heatmap(cm, annot=True,annot_kws={"size": 10})# font size
    plt.title(title)
    plt.show()
    print()


def test_tree(model_name, test_data, test_label, verbose=True, print_cm=False):
    from sklearn.tree import DecisionTreeClassifier
    clf_dtc = DecisionTreeClassifier()
    clf_dtc = joblib.load('data/' + model_name)

    prediction = clf_dtc.predict(test_data)
    acc = accuracy_score(test_label, prediction)
    if verbose:
        print("ACC for tree =", acc * 100, '%')
        if print_cm:
            print_statistics(test_label, prediction, "tree")

    return acc


def test_back(model_name, test_data, test_label, verbose=True, print_cm=False):
    from sklearn.neural_network import MLPClassifier
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(50,40,20), random_state=1)
    clf = joblib.load('data/' + model_name)

    prediction = clf.predict(test_data)
    acc = accuracy_score(test_label, prediction)
    if verbose:
        print("ACC for MLPC =", acc * 100, '%')
        if print_cm:
            print_statistics(test_label, prediction, "MLP")

    return acc


def test_forest(model_name, test_data, test_label, verbose=True, print_cm=False):
    from sklearn.ensemble import RandomForestClassifier

    clf = RandomForestClassifier(n_estimators=100)
    clf = joblib.load('data/' + model_name)

    prediction = clf.predict(test_data)
    acc = accuracy_score(test_label, prediction)
    if verbose:
        print("ACC for forest =", acc * 100, '%')
        if print_cm:
            print_statistics(test_label, prediction, "forest")

    return acc


def test_vote(model_name, test_data, test_label, verbose=False, print_cm=False):

    from sklearn.ensemble import VotingClassifier

    clf = joblib.load('data/' + model_name)

    prediction = clf.predict(test_data)
    acc = accuracy_score(test_label, prediction)
    if verbose:
        print("ACC for vote =", acc * 100, '%')
        if print_cm:
            print_statistics(test_label, prediction, "vote")

    return acc


def evaluate(version):
    """prints confusion matrix for all models"""
    global data
    test_data = data[0:, 1:]
    test_label = data[0:, 0]

    if version == "with":
        from train import add_attributes
        test_data = add_attributes(test_data)

    test_forest("rfc_model_"+version+".sav", test_data, test_label, verbose=True, print_cm=True)
    test_back("mlp_model_"+version+".sav", test_data, test_label, verbose=True, print_cm=True)
    test_tree("dtc_model_"+version+".sav", test_data, test_label, verbose=True, print_cm=True)
    test_vote("vote_model_"+version+".sav", test_data, test_label, verbose=True, print_cm=True)


def best_feature(version):
    global data
    test_data = data[0:, 1:]
    test_label = data[0:, 0]

    if version == "with":
        from train import add_attributes
        test_data = add_attributes(test_data)

    clf = joblib.load('data/rfc_model_'+version+'.sav')

    prediction = clf.predict(test_data)
    acc = accuracy_score(test_label, prediction)
    print("ACC for forest =", acc * 100, '%')
    l = len(test_data[0])
    f_names = np.arange(0, l, 1).reshape((l,1))
    f_imp = clf.feature_importances_
    sum = 0
    for i in f_imp:
        sum+=i
    print(sum)

    features = np.append(f_names, f_imp.reshape((l,1)), axis=1)
    # features = np.sort(features, axis=0)
    features = features[features[:,1].argsort()]
    top_three = features[::-1][:3]
    for row in top_three:
        print(row[0],row[1])


def main(argv):
    """Main"""
    try:
        opts, args = getopt.getopt(argv, "h", ["evaluate=", "best_feature=","preprocess_data"])
    except getopt.GetoptError:
        print(
            'test.py [--evaluate <version>] [--best_feature <version>]')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt == "--evaluate":
            evaluate(arg)
        elif opt == "--best_feature":
            best_feature(arg)
        elif opt == "--preprocess_data":
            print("Preprocessing data")
            preprocess_data()


if __name__ == "__main__":
    print("LOADING: test data in ./data/mnist_test.csv")
    data = pd.read_csv('./data/mnist_test.csv').to_numpy()
    print("         test data loaded")
    main(sys.argv[1:])
