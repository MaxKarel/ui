import pandas as pd
import numpy as np
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
from sklearn.externals import joblib

import sys
import getopt
import time
import math


def print_help():
    print("This is help")


def preprocess_data(type):
    global data
    if type == "normal":
        pass
    elif type == "none":
        print("Why even bother???")
    else:
        print("Uknown option for data preprocessing -> pass")


def attribute_1(train_data):
    """ White pixels """
    new_attribute = []
    for sample in train_data:
        tmp = 0
        for i in range(260):
            tmp += 1 if sample[i] > 0 else 0
        tmp = tmp / len(sample)
        new_attribute.append([tmp])

    new_attribute = np.array(new_attribute)
    return new_attribute


def attribute_2(train_data):
    """ Average number of non-white pixels in top third of picture """

    new_attribute = []
    for sample in train_data:
        tmp = 0
        for i in range(260):
            tmp += 1 if sample[i] > 0 else 0
        new_attribute.append([tmp])

    new_attribute = np.array(new_attribute)
    return new_attribute


def attribute_3(train_data):
    """ Count non-white pixel """
    new_attribute = []
    for sample in train_data:
        tmp = 0
        for pixel in sample:
            tmp += 1 if pixel > 0 else 0
        new_attribute.append([tmp])

    new_attribute = np.array(new_attribute)
    return new_attribute


def attribute_4(train_data):
    new_attribute = []
    for sample in train_data:
        new_attribute.append([sample[350], sample[377], sample[433]])

    new_attribute = np.array(new_attribute)
    return new_attribute

def add_attributes(_data_):
    """Add new attributes to data"""
    print("Adding new attributes to data")
    new_attributes = np.empty([len(_data_), 0])
    new_attributes = np.append(new_attributes, attribute_1(_data_), axis=1)
    new_attributes = np.append(new_attributes, attribute_2(_data_), axis=1)
    new_attributes = np.append(new_attributes, attribute_3(_data_), axis=1)
    new_attributes = np.append(new_attributes, attribute_4(_data_), axis=1)

    _data_ = np.append(_data_, new_attributes, axis=1)
    print("Finished adding new attributes to data")
    print()
    return _data_


def test_attributes(retrain_flag):
    from test import test_tree, test_back, test_forest, test_vote

    NUMBER_OF_PASSES = 1
    CLASSIFIER_COUNT = 4
    global train_data
    result_without = np.zeros(CLASSIFIER_COUNT)
    print("LOADING: test data in ./data/mnist_test.csv")
    data = pd.read_csv('./data/mnist_test.csv').to_numpy()
    test_data = data[0:, 1:]
    test_label = data[0:, 0]
    print("         test data loaded")

    for i in range(NUMBER_OF_PASSES):
        print("******** RUNNING PASS", i, "********")
        if retrain_flag:
            pass
            # train_forest("without")
            # train_back("without")
            # train_tree("without")
            # train_vote("without")
        result_without[0] += test_forest("rfc_model_without.sav",
                                         test_data, test_label, verbose=False)
        result_without[1] += test_back("mlp_model_without.sav",
                                       test_data, test_label, verbose=False)
        result_without[2] += test_tree("dtc_model_without.sav",
                                       test_data, test_label, verbose=False)
        result_without[3] += test_vote("vote_model_without.sav",
                                       test_data, test_label, verbose=False)

    # Calculate average accuracy
    for i in range(CLASSIFIER_COUNT):
        result_without[i] /= NUMBER_OF_PASSES
        result_without[i] *= 100
    ############################################################################
    train_data = add_attributes(train_data)
    test_data = add_attributes(test_data)
    ############################################################################
    result_with = np.zeros(CLASSIFIER_COUNT)

    for i in range(NUMBER_OF_PASSES):
        print("******** RUNNING PASS", i, "********")
        if retrain_flag:
            train_forest("with")
            train_back("with")
            train_tree("with")
            train_vote("with")
        result_with[0] += test_forest("rfc_model_with.sav",
                                      test_data, test_label, verbose=False)
        result_with[1] += test_back("mlp_model_with.sav",
                                    test_data, test_label, verbose=False)
        result_with[2] += test_tree("dtc_model_with.sav",
                                    test_data, test_label, verbose=False)
        result_with[3] += test_vote("vote_model_with.sav",
                                    test_data, test_label, verbose=False)
    for i in range(CLASSIFIER_COUNT):
        result_with[i] /= NUMBER_OF_PASSES
        result_with[i] *= 100

    ## Print results ###########################################################
    print("classifier: without/with attribute")
    print("Forest:\t", "{0:.2f}".format(
        result_without[0]), "{0:.2f}".format(result_with[0]))
    print("MLP   :\t", "{0:.2f}".format(
        result_without[1]), "{0:.2f}".format(result_with[1]))
    print("tree  :\t", "{0:.2f}".format(
        result_without[2]), "{0:.2f}".format(result_with[2]))
    print("vote  :\t", "{0:.2f}".format(
        result_without[3]), "{0:.2f}".format(result_with[3]))


def train_back(type):
    from sklearn.neural_network import MLPClassifier

    start_time = time.time()
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5,
                        hidden_layer_sizes=(50, 40, 20), random_state=1)
    # Train
    print("TRAINIG: MLPClassifier", train_data.shape)
    clf = clf.fit(train_data, train_label)
    print("         Finished trainig MLPClassifier in", time.time() - start_time)
    # Save model
    joblib.dump(clf, 'data/mlp_model_' + type + '.sav')


def train_tree(type):
    from sklearn.tree import DecisionTreeClassifier

    start_time = time.time()
    clf = DecisionTreeClassifier(random_state=1)
    # Train
    print("TRAINIG: Decision tree classifier")
    clf = clf.fit(train_data, train_label)
    print("         Finished trainig dtc in", time.time() - start_time)
    # Save model
    joblib.dump(clf, 'data/dtc_model_' + type + '.sav')


def train_forest(type):
    from sklearn.ensemble import RandomForestClassifier
    start_time = time.time()
    clf = RandomForestClassifier(n_estimators=100, random_state=1)
    # Train
    print("TRAINIG: Random forest classifier")
    clf = clf.fit(train_data, train_label)
    print("         Finished trainig rfc in", time.time() - start_time)
    # Save model
    joblib.dump(clf, 'data/rfc_model_' + type + '.sav')


def train_vote(type):
    from sklearn.ensemble import VotingClassifier
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.neural_network import MLPClassifier

    start_time = time.time()
    try:
        clf1 = RandomForestClassifier(n_estimators=100, random_state=1)
        clf1 = joblib.load('data/rfc_model_' + type + '.sav')
        clf2 = MLPClassifier(solver='lbfgs', alpha=1e-5,
                             hidden_layer_sizes=(50, 40, 20), random_state=1)
        clf2 = joblib.load('data/mlp_model_' + type + '.sav')
        clf3 = DecisionTreeClassifier(random_state=1)
        clf3 = joblib.load('data/dtc_model_' + type + '.sav')
    except FileNotFoundError:
        print("FILE not found - unable to continue")
        return

    clf = VotingClassifier(estimators=[
        ('rfc', clf1), ('mlp', clf2), ('dtc', clf3)],
        voting='hard')
    # Train
    print("TRAINIG: Voting classifier")
    clf = clf.fit(train_data, train_label)
    print("         Finished trainig rfc in", time.time() - start_time)
    # Save model
    joblib.dump(clf, 'data/vote_model_' + type + '.sav')


def create_models(type):
    """Creates 4 models files in data directory for each possible classifier
    Variable type determines the name of the files, which is important when
    running testing script"""
    global train_data
    if type == "with":
        train_data = add_attributes(train_data)

    train_forest(type)
    train_back(type)
    train_tree(type)
    train_vote(type)


def main(argv):
    mode = ""
    retrain_flag = True  # indicates wheter we want to train all models in scenario anew

    try:
        opts, args = getopt.getopt(
            argv, "hr", ["create_models=", "test_attributes", "preprocess_data"])
    except getopt.GetoptError:
        print(
            'train.py [--create_models <version>] [--test_attributes] [--preprocess_data]')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt == "-r":
            print("Program will use already created models. WARNING: all models must be present in data folder otherwise will program fail")
            retrain_flag = False
        elif opt == "--create_models":
            mode = "create_models"
            create_models_type = arg
        elif opt == "--test_attributes":
            mode = "test_attributes"
        elif opt == "--preprocess_data":
            mode = "create_models"
        else:
            print("Unknown option")

    if mode == "test_attributes":
        print("Testing 3 new attributes. Run tests scripts to see results")
        test_attributes(retrain_flag)
    elif mode == "create_models":
        print("Creating all 4 models for final page. All parameters for" +
              " model constructors have to be manually changed in the source code")
        create_models(create_models_type)
    else:
        print("ERRR")


if __name__ == "__main__":
    print("LOADING: trainig data in ./data/mnist_train.csv")
    data = pd.read_csv('./data/mnist_train.csv').to_numpy()
    # Prepare data
    train_data = data[0:, 1:]
    train_label = data[0:, 0]
    print("         training data loaded")
    main(sys.argv[1:])
