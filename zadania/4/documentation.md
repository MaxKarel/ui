# Artificial intelligence for MNIST dataset
This document describes usage of python scripts used in recognizing handwritten
digits from MNIST data-set. It is structured into 5 sections:
* Setup: This chapter describes how to setup environment and run the
* Model evaluation: how to create 4 models and evaluate their quality with confusion matrix
* Attributes: how to create models with additional attributes. Their description and comparison with model without them
* Model combination: how to combine multiple classifiers into another
* Attribute analysis: how to check which attribute is most significant

In process we will bee using 4 classifiers:
```
  from sklearn.ensemble import VotingClassifier
  from sklearn.ensemble import RandomForestClassifier
  from sklearn.tree import DecisionTreeClassifier
  from sklearn.neural_network import MLPClassifier

  clf1 = RandomForestClassifier(n_estimators=100, random_state=1)
  clf2 = MLPClassifier(solver='lbfgs',
                      alpha=1e-5,
                      hidden_layer_sizes=(50, 40, 20),
                      random_state=1)
  clf3 = DecisionTreeClassifier(random_state=1)
  clf = VotingClassifier(estimators=[
        ('rfc', clf1), ('mlp', clf2), ('dtc', clf3)],
        voting='soft')
```

## Setup
First navigate to folder ```UI/zadania/4```

Then create and activate virtual environment
```
python -m venv venv
source venv/bin/activate
```
Install all requirements with ```pip install -r requirements.txt```

Unpackage ```data/mnist-in-csv.zip```

## 1. Model evaluation
In order to evaluate models we need to create and train them first . This can be done by running ```python backend/train.py --create_models <version>``` if **version** is **"with"** then attributes will be added to training data. But **version** of model is always appended to end of model file name. After there should be four new files with trained models in data folder.

To test them and see the statistics you can run ```python backend/test.py --evaluate <version>``` where **version** should have same value as when training the models. If **version** is **"with"** then attributes will be added to test data.
#### Sample output
Below are confusion matrices for all four types of classifiers without attributes. We can notice that our neural network had a lot of errors when guessing 7 or 4 (classifier thought it was 9). We can try to minimize its effect with adding attributes to data.

![Confusion metrics for random forest classifier](docu_img/1_forest.png =500x350)
![Confusion metrics for MLP classifier](docu_img/1_mlp.png =500x350)
![Confusion metrics for decision tree classifier](docu_img/1_tree.png =500x350)
![Confusion metrics for Vote classifier](docu_img/1_vote.png =500x350)
## 2. Attributes
Let's try to improve our models with additional attributes in data. We can compare models with and without attributes. First we need to create models without attributes ```python backend/train.py --create_models without``` then ```python backend/train.py --test-attributes```. This way we dont have to retrain models for data without attributes. Later if we want to only display result we can use previous command with flag ```-r``` this will disable training of models.
#### All attributes
Bellow is accuracy for four classifier types with different variations of attributes:
| Classifier type | No attributes | All attributes |Attribute #1|Attribute #2|Attribute #3|
|:--------------:|:-------------:|:--------------:|:----------:|:----------:|:----------:|
| Forest         |    96.99      |     96.87      |   96.85    |   96.85    |   97.13    |
| MLP            |    87.21      |     92.30      |   94.35    |   93.12    |   94.61    |
| tree           |    87.57      |     87.74      |   88.00    |   88.00    |   88.33    |
| Vote           |    92.19      |     94.32      |   94.99    |   94.66    |   95.27    |
## 3. Model combination
For model combination we choose VotingClassifier which is regarded as best option from scikit-learn. It has two option for voting style **hard/soft**. When using hard we get majority vote, which might be ideal because ```DecisionTreeClassifier``` is usually under-performing and might be overridden by better classifiers. Here is how we gathered the data
```
python backend/train.py --create_model soft
python backend/train.py --create_model hard
python backend/test.py --evaluate soft
python backend/test.py --evaluate hard
```
|Vote Type| Accuracy |
|:-------:|:--------:|
| Soft    |  92.19   |
| Hard    |  94.54   |
## 4. Attribute analysis
Run with ```python backend/test.py --best_feature <version>```
In the output we will see 3 best attributes/features/pixels for RandomForestClassifier.
```
377.0 0.008828990846765646
433.0 0.008589663832599488
350.0 0.007782981855348238
```
Lets try to duplicate them (add them as additional feature) and see whether there is any other improvement.
| Classifier type | No attributes |  3  attributes |Extra attributes|
|:--------------:|:-------------:|:--------------:|:--------------:|
| Forest         |    96.99      |     96.87      |   96.96        |
| MLP            |    87.21      |     92.30      |   94.90        |
| tree           |    87.57      |     87.74      |   87.88        |
| Vote           |    92.19      |     94.32      |   96.38        |

And we can notice that our model improved by significant amount in two models (MLP and VotingClassifier)
