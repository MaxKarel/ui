from helpers import *
import time

def solve_breadth(init_s, final_s):
    """Solve 8 puzzle using breadth first search"""
    # Initialize
    final_h = state_hash(final_s)  # Hash of final state
    if state_hash(init_s) == final_h:
        print("Final and initial states are identical")
        return
    init_n = create_node(init_s, None, None, 0)
    queue = explore(None, init_n["state"], 0)  # Queue of unexplored nodes
    explored = {state_hash(init_s): True}  # HashMap (dictionary) of explored nodes

    # Start algorithm
    while queue:
        next_n = queue.pop(0)  # Remove next node to explore from
        next_h = state_hash(next_n["state"])
        if final_h == next_h:  # Check for solution
            print("DONE")
            return next_n
        elif next_h in explored:
            pass
            # Since we know that states with identical layout are the same (they dont depend on previous operator.
            # And also path_cost is 1 we can assume that if was node already explored there is no shorter solution
            # Therefore we can pass the node and ignore it
            # print("BEEN HERE", len(explored))
        else:
            new_nodes = explore(next_n, next_n["state"], next_n["depth"])
            explored[next_h] = True
            for i in new_nodes:
                queue.append(i)
    print("during solution", len(explored), "nodes have been explored")
    return


def solve_bidirectional(init_s, final_s):
    """Solve 8 puzzle using bidirectional search"""
    # init
    init_n1 = create_node(init_s, None, None, 0)
    init_n2 = create_node(final_s, None, None, 0)

    # init queue and explored
    queue1 = explore(None, init_n1["state"], 0)
    queue2 = explore(None, init_n2["state"], 0)
    explored1 = {}
    explored1[state_hash(init_s)] = init_n1
    explored2 = {}
    explored2[state_hash(final_s)] = init_n2

    while queue1 or queue2:
        if queue1:
            next_n1 = queue1.pop(0)
            next_h1 = state_hash(next_n1["state"])
            if next_h1 in explored2:
                return create_solution(next_n1,explored1,explored2, True)
            elif next_h1 in explored1:
                pass
            else:
                new_nodes = explore(next_n1, next_n1["state"], next_n1["depth"])
                explored1[next_h1] = next_n1
                for i in new_nodes:
                    queue1.append(i)
        if queue2:
            next_n2 = queue2.pop(0)
            next_h2 = state_hash(next_n2["state"])
            if next_h2 in explored1:
                return create_solution(next_n2,explored1,explored2, False)
            elif next_h2 in explored2:
                pass
            else:
                new_nodes = explore(next_n2, next_n2["state"], next_n2["depth"])
                explored2[next_h2] = next_n2
                for i in new_nodes:
                    queue2.append(i)
    return


def compare(init_s, final_s):
    start_bi = time.time()
    solve_bidirectional(init_s, final_s)
    time_bi = time.time() - start_bi
    print("Time for bidirectional search:", time_bi)

    start_breadth = time.time()
    solve_breadth(init_s, final_s)
    time_breadth = time.time() - start_breadth
    print("Time for breadth search:", time_breadth)

if __name__ == "__main__":
    init_s, final_s = load_config("test_7.dat")
    solution = None
    # solution = solve_breadth(init_s, final_s)
    # solution = solve_bidirectional(init_s, final_s)
    compare(init_s, final_s)
    if solution is None:
        print("There is no solution or you are using compare function")
    else:
        print_result(solution, init_s, final_s)
