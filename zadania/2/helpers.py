from copy import deepcopy

SIZE_X = 0
SIZE_Y = 0


def check_input(init_s, final_s):
    """Checks the dimension of input matrices"""
    init_x = len(init_s)  # Size of state in X direction
    init_y = len(init_s[0])  # Size of state in Y direction
    for row in init_s:
        if init_y != len(row):
            print("ERR: Irregular initial state")
            quit(1)

    final_x = len(final_s)
    final_y = len(final_s[0])
    for row in final_s:
        if final_y != len(row):
            print("ERR: Irregular final state")
            quit(1)

    if init_x != final_x or init_y != final_y:
        print("ERR: Dimensions of initial and final states dont match")
        quit(1)
    elif init_x * init_y > 36:
        print("ERR: Solution too big")
        quit(1)
    else:
        SIZE_X = init_x
        SIZE_Y = init_y


def load_config(file_name):
    """Load configuration from file and return it as an array of initial and final state"""
    text_file = open(file_name, "r")
    initial_state = []
    final_state = []
    solution = False

    for line in text_file.readlines():
        line = line.replace('\n',"")
        if line == 'SOL':
            solution=True
        elif solution:
            final_state.append(list(map(int,line.split(','))))
        else:
            initial_state.append(list(map(int,line.split(','))))

    check_input(initial_state, final_state)

    text_file.close()
    return initial_state, final_state


def state_hash(state):
    """Calculate uid for state"""
    result = 0
    coeff = 0
    if SIZE_X * SIZE_Y < 10:
        base = 10
    elif SIZE_X * SIZE_Y < 100:
        base = 100

    for rows in state:
        for item in rows:
            result += int(item) * base**coeff
            coeff += 1
    return result


def create_node(state, parent, operator, depth):
    """Create simple directory representing node"""
    result = {"state": state,
              "parent": parent,
              "operator": operator,
              "depth": depth}
    return result


def find_index_space(state):
    """Finds X and Y position of empty space"""
    Y = 0
    for row in state:
        if 0 in row:
            X = int(row.index(0))
            return X,Y
        Y += 1


def transmute_s(state, direction, X, Y):
    """Simulates moving a tile to empty space. Returns changed state"""
    result = deepcopy(state)
    if direction == 0:
        tmp = result[Y][X]
        result[Y][X] = result[Y-1][X]
        result[Y-1][X] = tmp
        #result[X][Y], result[X][Y-1] = result[X][Y -1], result[X][Y]
    elif direction == 1:
        tmp = result[Y][X]
        result[Y][X] = result[Y][X+1]
        result[Y][X+1] = tmp
    elif direction == 2:
        tmp = result[Y][X]
        result[Y][X] = result[Y+1][X]
        result[Y+1][X] = tmp
    else:
        tmp = result[Y][X]
        result[Y][X] = result[Y][X-1]
        result[Y][X-1] = tmp
    return result


def explore(node_ptr, state, depth):
    """Returns list of nodes accessible from node_ptr"""
    result = []
    X, Y = find_index_space(state)
    for i in range(4):
        # Move up
        if i == 0 and Y > 0:
            new_state = transmute_s(state,0,X,Y)
            result.append(create_node(new_state, node_ptr, 0, depth+1))
        # Move right
        elif i == 1 and X < 2:
            new_state = transmute_s(state, 1, X, Y)
            result.append(create_node(new_state, node_ptr, 1, depth+1))
        # Move down
        elif i == 2 and Y < 2:
            new_state = transmute_s(state, 2, X, Y)
            result.append(create_node(new_state, node_ptr, 2, depth+1))
        # Move left
        elif i == 3 and X > 0:
            new_state = transmute_s(state, 3, X, Y)
            result.append(create_node(new_state, node_ptr, 3, depth+1))

    return result


def print_result(node, init_state, final_state):
    depth = 1
    for row in init_state:
        print(row)
    print("  DEPTH=",depth)
    while node is not None:
        for row in node["state"]:
            print(row)
        depth += 1
        print("  DEPTH=",depth)
        node = node["parent"]
    for row in final_state:
        print(row)
    depth += 1
    print("  DEPTH=", depth)


def create_solution(middle_n, explored1, explored2, direction):
    """Merges two linked lists from bidirectional search into one solution"""
    middle_h = state_hash(middle_n["state"])

    # Initialize parameters based on which direction of search finished first
    if direction:
        prev = middle_n["parent"]
        next = explored2[middle_h]
        depth = next["depth"] + 1
    else:
        next = middle_n["parent"]
        prev = explored1[middle_h]["parent"]
        depth = next["depth"] + 1

    # Change direction of linked list pointing from middle_node to initial_state
    while prev is not None:
        tmp = prev["parent"]
        prev["parent"] = next
        prev["depth"] = depth
        next = prev
        prev = tmp
        depth += 1
    return next
