# Obojsmerné prehľadávanie
Cielom tejto dokuemntácie je vysvetliť implementáciu riešenia problému 8-hlavolamu

## Spustenie
Na spustenie je potrebné použíť príkaz:
`python3.7 main.py`

## Priebeh
### Inicializácia
Na začiatok sú načitané počiatočné a koncové podmienky hlavolamu.
Tie sú pomocou funkcie `check_input()` skontrolované. Dôležité je aby aby išlo o zoznamy formátu __NxM__ a aby oba mali rovnakú veľkosť. Taktiež sú odseparované príliš veľké verzie hlavolamu, ktorých stavový priestor je príliš veľký.

Následne sú oba stavy poslané do funkcie `solve_bidirectional()` ktorá vráti stav riešenia nasledujúci hneď po počiatočnom.

Existuje aj jednoduchšia funkcia, ktorá prehľadáva došírky a použili sme ju na testovacie účely


### Priebeh hľadania riešenia

Notácia použitá je, že všetky premenné končiace na __1__ reprezentujú smer hladania od počiatku (Tie s __2__ zas reprezenujú hladanie od cieľového stavu)

Na začiatku sú vytvorené dva vrcholy pre oba smery a sú rozvinuté funkciou `explore()`. Do hashmapy `explored` sú počiatočné stavy pridané pre prípad, že by počiatočný a cieľový stav boli identické. Tieto množiny zabezpečia, že zistíme kedy sa oba smery hladania stretly. Kedže ide o neinformované hladanie a spätný krok je prípustný (aj keď je očividné, že je zbytočný) potrebujeme dve množiny iž preskúmaných uzlov. Riešením by bolo Zamedziť spätnému kroku alebo pridať k hodnote v mape parameter popisujúci ktorý smer tento stav preskúmal. To by však narástla pamäťová náročnosť a aj zložitosť kontroly či riešenie bolo dosiahnuté.

Nasledujúci cyklus postupne vyberá prvky z `queue1` a `queue2` a rozvíja ich. Nové vrcholy sú pomocou `append()` pridané na koniec radu.

### Funkcia `explore`
Jej výstupom je zoznam s vrcholmy, ktoré obsahulú nové stavy. Je samozrejme kontroovaná legalita ťahu (medzeru nemôžeme úresunuť za hranice hracej plochy). Novovytvorený stav získame z funkcie `transmute` ktorá len vytvorí novú kópiu poľa a vymení príslušné elementy.

### Výsledné riešenie
Keď zistíme, že jeden smer už rozvniul uzol, ktorý sme práve dostali z `queue` vieme že sme našli riešenie. Teraz však máme dva uzly s identickým stavom, ktoré ukazujú cestu ako sa dostať do počiatočného a začiatočného uzla. Mi však potrebujeme len ceztu zo začiatku na koniec. Preto `while` cyklom zmeníme smer spájaného zoznamu stavov tak aby sme vypisovacej funkcíí mohli odovzdať prvý krok riešenia.

## Testovanie
* Testy 1-4 boli použité na debugovanie
* Test 5 na overenie vačšieho rozmeru
* Test 6 na overenie prípadu, keď __N != M__
* Test 7 sluzil na casovanie
  * Time for bidirectional search: 408.5
  * Time for breadth search: 179.9
  * Vysledky vsak nesedia s teoretickym predpokladom vyhodnosti obojsmerneho hladania a dovod sa nam nepodarilo objavit
