import random
import operator
import copy


def load_config(file_name):
    """Load configuration from file and return it as a dictionary of parameters"""
    text_file = open(file_name, "r")
    result = {}

    line = text_file.readline()
    width, height = map(int, line.split(','))
    result['X'], result['Y'] = width, height

    result['STONES'] = list()
    for row_num, line in enumerate(text_file.readlines()):
        line = line.replace('\n', "").split(",")
        # Load stone positions from map
        try:
            col_numbers = [i for i, n in enumerate(line) if n == 's']
            for column in col_numbers:
                result['STONES'].append((column, row_num))
        except ValueError:
            pass

    # Write down constants describing generation
    result['GEN_SIZE'] = 100
    result['CHR_SIZE'] = height * width + len(result['STONES'])
    result['CHR_RANGE'] = 2 * width + 2 * height - 1
    result['BLOCK_SIZE'] = result['CHR_SIZE'] // 10

    text_file.close()
    return result


def print_result(duration, generations_n=0, generation_s=0):
    """Print results after each run"""
    print("Duration =", duration)
    print("Number of generations =", generations_n)
    print("Generation size =", generation_s)


def print_garden(garden):
    """Debuging function to print out garden nicely"""
    for row in garden:
        for tile in row:
            print(tile, end=", ")
        print()
    print()


def print_generation(generation, size=0):
    """Debuging function prints whole generation"""
    if 0 < size < len(generation):
        for i in range(size):
            print(generation[i]['fit'], generation[i]['chr'])
    else:
        for value in generation:
            print(value['fit'], value['chr'])


def calc_progress(generation, size=0, number_of_gen = 0):
    """Function prints out all relevant information about generation after fitness has been assessed"""
    total = 0
    if 0 < size < len(generation):
        for member in generation[0:size]:
            total += member['fit']
        average_fitness = total / size
    else:
        for member in generation:
            total += member['fit']
        average_fitness = total / len(generation)
    average_fitness = round(average_fitness, 3)
    max_fitness = round(generation[0]['fit'], 3)
    print(number_of_gen,':','Top', size, "AVG=", average_fitness, ", MAX=", max_fitness)


def init_garden(parameters):
    """Initializes empty garden with stones"""
    size_x = parameters['X']
    size_y = parameters['Y']
    garden = [[0]*size_x for i in range(size_y)]
    for stone in parameters['STONES']:
        garden[stone[1]][stone[0]] = 's'
    return garden


def init_generation(parameters):
    """Initialize first generation with random genes"""
    result = []
    chromosome_size = parameters['CHR_SIZE']
    chromosome_range = parameters['CHR_RANGE']
    generation_size = parameters['GEN_SIZE']
    for i in range(generation_size):
        member = {}
        chromosome = []
        for j in range(chromosome_size):
            chromosome.append(random.randint(0, chromosome_range))
        member['chr'] = chromosome.copy()
        member['fit'] = 0
        result.append(member)
    return result


def starting_position(index, parameters):
    """Function transforms index of path around the garden to (X,Y) of first field in one sweep of garden
    (one gene execution). Also returns tuple indicating direction in which to start sweeping the garden"""
    X = parameters['X']
    Y = parameters['Y']
    # Since we surround the garden with path and exclude corners we know that its 2*X + 2*Y long.
    # Then we can divide path into 4 segments. That will help us determine starting direction in problematic corners.
    if 0 <= index < X:
        garden_x = index
        garden_y = 0
        direction = [0, 1]
    elif X <= index < X + Y:
        garden_x = X - 1
        garden_y = index - X
        direction = [-1, 0]
    elif X + Y <= index < 2 * X + Y:
        garden_x = X - (index - (X + Y)) - 1
        garden_y = Y - 1
        direction = [0, -1]
    elif 2 * X + Y <= index < 2 * (X+Y):
        garden_x = 0
        garden_y = Y - (index - (2 * X + Y)) - 1
        direction = [1, 0]
    else:
        print("ERR: INVALID INDEX")
    return garden_x, garden_y, direction


def calculate_fitness(garden):
    """Calculates percentage of how much of the garden was explored. Stones count as if they were explored tiles."""
    unexplored = 0
    for row in garden:
        unexplored += row.count(0)
    total = len(garden) * len(garden[0])
    explored = total - unexplored
    return explored / total


def rotate_right(d):
    """Rotates direction 90 degrees to right"""
    tmp_x = d[0]
    d[0] = d[1] * (-1)
    d[1] = tmp_x


def rotate_left(d):
    """Rotates direction 90 degrees to left"""
    tmp_y = d[1]
    d[1] = d[0] * (-1)
    d[0] = tmp_y


def move(parameters, garden, x, y, d, i):
    """Execute movement of one gene."""
    initial_x = x
    initial_y = y
    if garden[y][x] != 0:
        # Initial tile was already blocked
        return False
    turn_count = 0
    while 0 <= x < parameters['X'] and 0 <= y < parameters['Y']:
        if(garden[y][x]) == 0:
            garden[y][x] = i
            turn_count = 0
        else:
            x -= d[0]
            y -= d[1]
            if ((x+y) % 2) == 0:
                rotate_right(d)
            else:
                rotate_left(d)
            turn_count += 1
            if turn_count == 4:
                # Monk is stuck in the middle of garden
                return False
        x += d[0]
        y += d[1]
    x -= d[0]
    y -= d[1]
    if turn_count != 0 and initial_x == x and initial_y == y:
        # If monk enters and leaves from the same tile
        return False
    return True


def simulate(member, parameters):
    """Simulate movement in Zen garden and assign fitness number"""
    garden = init_garden(parameters)
    for i, start in enumerate(member['chr']):
        x, y, d = starting_position(start, parameters)
        if not move(parameters, garden, x, y, d, i + 1):
            # replace illegal path with 0
            # for row in garden:
            #     for j in range(len(row)):
            #         if row[j] == i+1:
            #             row[j] = 0
            # End execution after illegal move
            break
    fit = calculate_fitness(garden)
    member['fit'] = fit


def fitness(generation, parameters):
    """Function will simulate each member of generation and assign fitness number to each one
    Based on how much percent of garden are traversed"""
    for member in generation:
        simulate(member, parameters)
    generation.sort(key=operator.itemgetter('fit'), reverse=True)
    if generation[0]['fit'] == 1.0:
        return True
    else:
        return False


def mutate(parameters, parent):
    """Mutate 10% of genome size to random number"""
    result = parent.copy()
    chromosome_size = parameters['CHR_SIZE']
    chromosome_range = parameters['CHR_RANGE']
    for i in range(chromosome_size // 10 ):
        pos = random.randint(0, chromosome_size - 1)
        result['chr'][pos] = random.randint(0, chromosome_range)
    return result


def combine(parameters, parent1, parent2):
    """Combine random blocks of genes from two parents. Block size is defined at start, in parameters"""
    result = {'chr': parent1['chr'].copy(), 'fit': 0}

    chromosome_size = parameters['CHR_SIZE']
    block_size = parameters['BLOCK_SIZE']
    block_count = int(chromosome_size // block_size)
    if block_count == 0:
        print("BLOCK TOO SMALL")
    for i in range(block_count):
        parent = random.randint(0, 1)
        if parent == 1:
            start = i*block_size
            end = (i+1)*block_size
            pblock = parent2['chr'][start:end].copy()
            result['chr'][start:end] = pblock
    return result


def roulette(old_gen, parameters):
    total_sum = 0
    for key in old_gen:
        total_sum += key['fit']
    total_sum = random.uniform(0,1) * total_sum
    for i, key in enumerate(old_gen):
        total_sum -= key['fit']
        return i
    return len(old_gen) - 1


def reproduce(old_gen, parameters):
    """Create new generation from old one"""
    # Move first 10 members of generation to new one
    new_gen = copy.deepcopy(old_gen[:10])
    for i in range(parameters['GEN_SIZE'] - 10):
        reproduction_type = random.randint(0, 1)
        # parent1 = random.randint(0, 9)
        # parent2 = random.randint(0, 9)
        parent1 = roulette(old_gen, parameters)
        parent2 = roulette(old_gen, parameters)
        if reproduction_type == 0:
            new_gen.append(mutate(parameters, old_gen[parent1]))
        elif reproduction_type == 1:
            new_gen.append(combine(parameters, old_gen[parent1], old_gen[parent2]))
        else:
            print("PROBLEM")
    return new_gen
