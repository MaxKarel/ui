from helpers import *
import time


def solve():
    start = time.time()
    # INIT
    generation = init_generation(parameters)
    number_of_generations = 0
    # MAIN
    for iteration in range(1000):
        # FITNESS and TEST
        if fitness(generation, parameters):
            # If we found member with fitness = 1.0
            break
        # Print progress
        #calc_progress(generation, 15, number_of_generations)
        # REPRODUCE and MUTATE
        generation = reproduce(generation, parameters)
        number_of_generations += 1

    duration = time.time() - start
    print_result(duration, number_of_generations, parameters['GEN_SIZE'])
    if generation[0]['fit'] == 1.0:
        print("perfect", generation[0]['fit'])
    return generation[0]['fit']


if __name__ == "__main__":
    parameters = load_config("test_0.dat")
    total = 0
    for i in range(100):
        total += solve()
    print(total / 100)
