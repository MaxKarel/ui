# Genetický algoritmus
V dokumentácií je popísaný genetický algoritmus určený na prehladávenie Zenovej záhrady.
Je rozdelený na tri časti:
* Popis génov: V tejto časti je vysvetlené ako sú gény interpretované a ako vznikajú
* Popis algoritmu a kríženia: Detailný popis fungovania kríženia a algortimu
* Testovacie scenáre a výsledky

## Popis génov
Gény v našom algoritme popisujú začiatočné body vstupu mnícha do záhrady. Aby sme deterministicky vedeli určiť ktorým smerom sa má v rohu záhrady vybrať (Ak je začiatočný bod napríklad (0,0) nevieme či má ísť vodorovne alebo zvyslo). Preto génom reprezentujeme index cesty, ktorá vedie okolo záhrady a začína v ľavom hornom rohu s indexom 0. Kedže nemôžme začať hrabať záhradu z cesty v rohu tieto polia ignorujeme. Tým pádom máme cesti dlhú `C=2X+2Y` políčok.
![Znázornenie záhrady](IMG_garden.jpg)

Preto pri generácií nového génu nám stačí vybrať náhodné číslo z intervalu **0..C**. Každý jedinec má `G = XY + S` génov, kde **S** je počet kameňov na mape.

## Popis algoritmu
Hlavná iterácia prejde maximálne 1000 generácií a skladá sa z 3 procedúr:
* Ohodnotenie (`fintess()`): Tu je každému jedincovy pridelená jeho fitness hodnota, ktorá hovorí kolǩo zo záhrady pohrabal. Ak táto funkcia vráti Pravdu vieme, že sme našli jedinca, ktorý pohrabal celú záhradu a program môže skončiť
* Výpis (`calc_progress()`): Slúži len na popis generáci a jej priemernej fitness hodnoty. Je len informatívna
* Reprodukcia (`reproduce()`): Vytvorenie novej generácie zo starej

### `fitness()`

Na princípe počítania fitness funkcie nieje veľa čo vysvetliť.
Pre každého jedinca je zavolaná funkcia `simulate()` a na záver sú jedinci zoradený zostupn podľa hodnoty fitness.

### `simulate()`

Pre každého jedinca v generácií vytvoríme prázdnu záhradu s kameňmi
Následne z génu zistíme jeho začiatočnú pozíciu a smer.
Potom už stačí zaznačit cestu mnícha do záhrady. Ak by funckia `move()` vrátila **false** vieme, že mních vykonal neoprávnený pohyb.
V takom prípade musíme nahradiť jeho ilegálne kroky 0. Definovali sme si totiž, že problém budeme považovať za vyriešený iba ak je pohrabaná záhrada a mních skončil na kraji.

### `reproduce()`
Najprv je 10 najlepŠích jedincov z predošlej generácie presunutý do novej. Následne sa s 50% šancou vytvorí nový jedinec jednou z nasledujúcich možností:
* `mutate()`: Dôjde k zmutovaniu 10 percent genetickej zásoby jedného náhodného rodiča
* `combine()`: Dôjde ku kombinácií dvoch náhodných rodičov. Kopírovanie génov sa deje po nahodne vybraných blokoch. Veľkosť bloku je definovaná na začiatku v parametroch.


## Testovanie a výsledky

Na testovanie sme použili primárne dve záhrady. Prvá `test_0.dat` je kópiou záhrady zo zadanie. Druhá záhrada `test_1.dat` je výrazne jednoduchšia záhrada s menším počtom kameňov.

#### **test_0.dat**

Ukážka výstupu pre mapu zo zadania:

    Top 15 AVG= 0.57 , MAX= 0.708
    Top 15 AVG= 0.634 , MAX= 0.708
    Top 15 AVG= 0.702 , MAX= 0.708
    Top 15 AVG= 0.725 , MAX= 0.792
    Top 15 AVG= 0.735 , MAX= 0.792
    Top 15 AVG= 0.741 , MAX= 0.792
    Top 15 AVG= 0.792 , MAX= 0.808
    Top 15 AVG= 0.805 , MAX= 0.808
    Top 15 AVG= 0.808 , MAX= 0.808
    Top 15 AVG= 0.811 , MAX= 0.85
    Top 15 AVG= 0.823 , MAX= 0.85
    Top 15 AVG= 0.829 , MAX= 0.85
    Top 15 AVG= 0.85 , MAX= 0.85
    ....
    Top 15 AVG= 0.933 , MAX= 0.933
    Duration = 5.182366609573364
    Number of generations = 1000
    Generation size = 100

Rozdiel medzi mutáciou 10% a 20% génov. Posledné číslo je priemerná hodnota najlepšieho jedinca po sto iteráciach. Dôvodom tohto riešenia je, že väčšinou naše riešenia skončia v lokálnom maxime a nepodarí sa nám pohrabať celú záhradu.

| Pokus |  10%  |  20%  |  33%  |
|:-----:|:-----:|:-----:|:-----:|
| 1     | 0.942 | 0.925 | 0.958 |
| 2     | 0.958 | 0.992 | 0.933 |
| 3     | 0.992 | 0.942 | 0.916 |
| 4     | 0.925 | 0.975 | 0.933 |
|100(Priemer)|0.9528|0.9522|0.9383|

Avšak vidíme, že zásadný rozdiel na priemernú hodnotu najlepšieho jedinca to nemá. Dokonca pri mutovaní tretiny génov sa priemer zhoršil.


#### **test_1.dat**

Kedže v prvom príklade sa nám v menšom počte pokusov nepodarilo nájsť perfektné riešenie skúsili sme jednoduchšiu záhradu. Tu sa nám v 100 pokusoch podarilo násjť 14 perfektných riešení. Priemerná úspešnosť najlepšieho jedinca každej generácie vzrástla oproti prvému testu na 0.974.

#### **Po implementácií rulety**

Pokúsili sme sa dostať z lokálneho minima implementovaním rulety. Pre 100 iterácií na mape test_0.dat pre rôzne percentá mutovaných génov.

|  100 pokusov   | 10% | 20% |
| #spravnych| 2 | 3 |
|Priemer| 0.9552 | 0.9472 |

Vidíme, že aj napriek tomu že celkový priemer ostal rovnaký ako v predchádzajúcich prípadoch občas sa nám podarilo nájsť správne riešenie.
